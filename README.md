# timeline


Pre-requisite:
- nodejs

Installation instructions:


```sh
git@gitlab.com:sai.b.satchitanand/itinerary.git
npm install
```

For development
```sh
yarn dev
```

or to run application
```sh
yarn build
yarn start
```

Technologies used
- [React](https://reactjs.org/)
- [Typescript](https://www.typescriptlang.org/)
- [Nextjs](http://nextjs.org) - SSR framework for react, out of the box routing and easy customization using different plugins or writing our own without *eject*.
- [mobx](https://mobx.js.org/) - state management.
- [antd](https://ant.design/) Design system with amazing components.
- [Jest](https://jestjs.io/)
- [react-testing-library](https://testing-library.com/docs/react-testing-library/api) For component testing.
- [styled jsx](https://github.com/zeit/styled-jsx) For scoping CSS.
- [postcss](https://postcss.org/) For transforming CSS.
- [commitzen](http://commitizen.github.io/cz-cli/) Standardizing commit messages.