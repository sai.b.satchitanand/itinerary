const withCss = require('@zeit/next-css');

if (typeof require !== 'undefined') {
  require.extensions['.css'] = file => {};
}

const compose = (...fns) => (
  fns.reduceRight((prevFn, nextFn) => (...args) => nextFn(prevFn(...args)), value => value)
);

const withDecorators = compose(
  withCss,
);

module.exports = withDecorators({
  webpack: config => {
    // Fixes npm packages that depend on `fs` module
    config.node = {
      fs: 'empty',
    }

    return config;
  }
});
