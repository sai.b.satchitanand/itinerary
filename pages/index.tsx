import React, { useEffect } from 'react';
import { Layout, Icon } from 'antd';

import { TimeLineWrapper } from '../src/components/timeline/TimeLineWrapper';
import ItineraryStore from '../src/stores/ItineraryStore';
import { ItineraryStoreContext } from '../src/context';

const { Header, Content } = Layout;
const Home: React.FunctionComponent = () => {
  const itineraryStore = ItineraryStore.getInstance();

  useEffect(() => {
    itineraryStore.getItinerary();
  },        []);

  return (
    <ItineraryStoreContext.Provider value={itineraryStore}>
      <Layout>
        <Header>
          <Icon type="left" />
          Overview
        </Header>
        <Content>
          <TimeLineWrapper />
        </Content>
      </Layout>
      <style jsx>{`
        :global(
          .ant-layout-content
        ) {
          padding: 10px 20px;
        }
        :global(
          .ant-layout-header
        ) {
            padding-left: 20px;
            color: #fff;
            font-size: 20px;
          }
      `}
      </style>

    </ItineraryStoreContext.Provider>
  );
};

export default Home;
