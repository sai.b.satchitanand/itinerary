import React from 'react';
import { Card } from 'antd';

import { Segment, SegmentType } from '../../../types/itinerary';

type TimeCardProps = {
  segment: Segment,
};

function TimeCard({
  segment,
}: TimeCardProps) {
  switch (segment.type) {
    case SegmentType.GROUND:
      return (
        <Card>
          <p>Pick up from</p>
          <p>{segment.origin.value}</p>
        </Card>
      );

    default:
      return (
        <Card>
          <p>Card content</p>
        </Card>
      );
  }
}

export { TimeCard };
