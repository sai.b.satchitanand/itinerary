import React from 'react';
import { render, cleanup } from '@testing-library/react';

import ItineraryStore from '../../stores/ItineraryStore';
import { ItineraryStoreContext } from '../../context';
import { TimeLineWrapper } from './TimeLineWrapper';

const store = ItineraryStore.getInstance();

describe('TimeLineWrapper:', () => {
  afterEach(cleanup);

  it('should show spinner when there is no data', () => {
    const { getByTestId } = render(
      <ItineraryStoreContext.Provider value={store}>
        <TimeLineWrapper />
      </ItineraryStoreContext.Provider>,
    );

    expect(getByTestId('spinner')).toBeDefined();
  });
});
