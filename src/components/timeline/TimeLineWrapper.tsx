import React, { useContext } from 'react';
import { Steps, Spin, Icon } from 'antd';
import { observer } from 'mobx-react-lite';

import { ItineraryStoreContext } from '../../../src/context';
import { TimeCard } from './TimeCard';

const { Step } = Steps;
const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;
const formatTime = (timestamp: number) => new Date(timestamp)
  .toTimeString()
  .slice(0, 5);

const TimeLineWrapper: React.FunctionComponent = observer(() => {
  const itineraryStore = useContext(ItineraryStoreContext);

  if (!itineraryStore.itinerary) {
    return <Spin indicator={antIcon} data-testid="spinner" />;
  }

  return (
    <div className="timeline">
      <div className="header">
        {itineraryStore.itineraryHeader}
      </div>
      <Steps direction="vertical" progressDot current={1}>
        {
          itineraryStore
            .itinerary
            .segments
            .map((segment) => {
              return (
                <Step
                  key={segment.segment_id}
                  title={formatTime(segment.departure)}
                  description={
                    <TimeCard
                      segment={segment}
                    />
                  }
                />
              );
            })
        }
      </Steps>`
      <style jsx>{`
        .header {
          padding-left: 23px;
          padding-bottom: 10px;
          font-size: 16px;
        }
        .timeline :global(.ant-steps-dot .ant-steps-item-content) {
          width: auto;
        }
      `}
      </style>
    </div>
  );
});

export { TimeLineWrapper };
