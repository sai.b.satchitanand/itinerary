import React from 'react';
import ItineraryStore from './stores/ItineraryStore';

export const ItineraryStoreContext = React.createContext<ItineraryStore>(
  ItineraryStore.getInstance(),
);
