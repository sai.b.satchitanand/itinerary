import { observable, action, computed } from 'mobx';

import { Itinerary } from '../../types/itinerary';
import isServer from '../utils/isServer';

const translations = {
  TXL: 'Berlin',
  MUC: 'München',
};

export default class ItineraryStore {
  private static instance: ItineraryStore;
  @observable itinerary: Itinerary;

  static getInstance() {
    if (isServer() || this.instance === undefined) {
      return this.instance = new this();
    }
    return this.instance;
  }

  @computed
  get itineraryHeader() {
    return `
      ${translations[this.itinerary.origin_iata]} - ${translations[this.itinerary.destination_iata]}
    `;
  }

  @action
  getItinerary() {
    return fetch('/api/itinerary')
      .then(res => res.json())
      .then((res: Itinerary) => {
        this.itinerary = res;
      });
  }
}
