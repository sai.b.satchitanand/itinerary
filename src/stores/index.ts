import { configure } from 'mobx';
import { useStaticRendering } from 'mobx-react-lite';
import ItineraryStore from './ItineraryStore';

useStaticRendering(true);
configure({
  enforceActions: 'observed',
});

export default {
  ItineraryStore,
};
